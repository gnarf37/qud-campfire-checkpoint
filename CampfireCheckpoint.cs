using System;
using System.Threading;
using XRL;
namespace XRL.World.Parts
{

    public class Gnarf_CampfireCheckpoint : IPart
    {
        [NonSerialized]
        private bool? _enabled = null;
        public bool IsEnabled()
        {
            if (_enabled == null)
            {
                _enabled = CheckpointingSystem.IsCheckpointingEnabled() && The.Game.GetStringGameState("Gnarf_CampfireCheckpoint") == "Enabled";
            }
            return (bool)_enabled;
        }

        public override bool WantEvent(int ID, int cascade) =>
            (IsEnabled() && (
                ID == InventoryActionEvent.ID ||
                ID == GetCookingActionsEvent.ID
            )) ||
            (!IsEnabled() && (
                ID == ObjectCreatedEvent.ID
            )) ||
            base.WantEvent(ID, cascade);

        public override bool HandleEvent(ObjectCreatedEvent E)
        {
            // game mode can't change - remove our part from objects so we dont mess with save compat when disabled
            if (!IsEnabled()) ParentObject.RemovePart(this);
            return base.HandleEvent(E);
        }

        public override bool HandleEvent(GetCookingActionsEvent E)
        {
            if (IsEnabled()) E.AddAction("Checkpoint", "Set checkpoint", "Checkpoint", Key: 'c', Default: 0, Priority: 200);
            return base.HandleEvent(E);
        }
        public override bool HandleEvent(InventoryActionEvent E)
        {
            if (IsEnabled() && E.Command == "Checkpoint")
            {
                AddPlayerMessage("Checkpointing...");
                CheckpointingSystem.DoCheckpoint();
                E.RequestInterfaceExit();
            }
            return base.HandleEvent(E);
        }

    }
}